import {Component, Injector, ViewEncapsulation} from '@angular/core';
import {AppComponentBase} from '@shared/common/app-component-base';
import {DashboardCustomizationConst} from '@app/shared/common/customizable-dashboard/DashboardCustomizationConsts';
import {NzI18nService, en_US} from 'ng-zorro-antd';
import {ja} from 'date-fns/locale';

@Component({
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.less'],
    encapsulation: ViewEncapsulation.None
})

export class DashboardComponent extends AppComponentBase {
    dashboardName = DashboardCustomizationConst.dashboardNames.defaultTenantDashboard;

    constructor(
        private i18n: NzI18nService,
        injector: Injector) {
        super(injector);
    }

    changeLang() {
        en_US.Calendar.dayFormat = 'mm/DD/YYYY';
        en_US.DatePicker.lang.dateFormat = 'mm/DD/yyyy';
        en_US.DatePicker.lang.dateTimeFormat = 'mm/DD/yyyy';
        en_US.DatePicker.lang.today = 'hihi';
        this.i18n.setLocale(en_US);
    }
}
