﻿using System;
using Abp.UI.Inputs;

namespace newPSG.PMS.CustomInputTypes
{
    /// <summary>
    ///Multi Select Combobox value UI type.
    /// </summary>
    [Serializable]
    [InputType("MULTISELECTCOMBOBOX")]
    public class MultiSelectComboboxInputType : InputTypeBase
    {
    }
}
