﻿namespace newPSG.PMS.Authorization.Delegation
{
    public interface IUserDelegationConfiguration
    {
        bool IsEnabled { get; set; }
    }
}