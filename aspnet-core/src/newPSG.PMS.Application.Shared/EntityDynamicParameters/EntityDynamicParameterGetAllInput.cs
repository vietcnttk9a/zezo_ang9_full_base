﻿namespace newPSG.PMS.EntityDynamicParameters
{
    public class EntityDynamicParameterGetAllInput
    {
        public string EntityFullName { get; set; }
    }
}
