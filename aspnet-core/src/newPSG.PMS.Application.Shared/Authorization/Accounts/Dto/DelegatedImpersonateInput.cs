namespace newPSG.PMS.Authorization.Accounts.Dto
{
    public class DelegatedImpersonateInput
    {
        public long UserDelegationId { get; set; }
    }
}