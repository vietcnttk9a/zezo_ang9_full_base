﻿namespace newPSG.PMS.EntityDynamicParameterValues.Dto
{
    public class GetAllInput
    {
        public string EntityId { get; set; }

        public int ParameterId { get; set; }
    }
}
