# ASP.NET ZERO

This repository is configured and used for AspNet Zero Team's development. 
It is not suggested for our customers to use this repository directly. It is suggested to download a project from https://aspnetzero.com/Download.

____________

## Most Recent Release

|  #   |     Status     |  Release Date  |                         Change Logs                          |                          Milestone                           |
| :--: | :------------: | :--------: | :----------------------------------------------------------: | :----------------------------------------------------------: |
| 8.6 | ✔️ **RELEASED** | 2020-04-20 | [Release Notes](https://docs.aspnetzero.com/en/common/latest/Change-Logs) | [Closed](https://github.com/aspnetzero/aspnet-zero-core/milestone/68?closed=1) |

## Current Milestone
|  #   |    Status     | Due Date |                          Milestone                           |
| :--: | :-----------: | :------: | :----------------------------------------------------------: |
| 8.7  | 🚧 In Progress |2020-05-07| [Open](https://github.com/aspnetzero/aspnet-zero-core/milestone/69)<br>[Closed](https://github.com/aspnetzero/aspnet-zero-core/milestone/69?closed=1) |

